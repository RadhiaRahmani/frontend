import "./assets/css/style.css";
import "./assets/vendors/mdi/css/materialdesignicons.min.css";
import "./assets/vendors/css/vendor.bundle.base.css";
import "./App.css";
import NavbarPage from "./Components/NavbarPage";
import SidebarPage from "./Components/SidebarPage";
import FooterPage from "./Components/FooterPage";

import UserPage from "./Views/User/UserPage";
import EmployerPage from "./Views/Employer/EmployerPage";
import ProfilePage from "./Views/Profile/ProfilePage";

import { Route, Routes, Navigate } from "react-router-dom";
import LoginPage from "./Views/Account/LoginPage";
import Addadmin from "./Views/Admin/Addadmin";
import Adduser from "./Views/User/Adduser";
import Adminpage from "./Views/Admin/Adminpage";
function App() {
  return (
    // <div class="container-scroller rtl">
    //   {/* <NavbarPage /> */}
    //   <div class="container-fluid page-body-wrapper">
    //     {/* <SidebarPage /> */}
    //     <div class="main-panel">
    <div class="content-wrapper">
      <Routes>
        <Route path="/" element={<Addadmin />} />
        <Route path="/User" element={<UserPage />} />
        {/* <Route path="/" element={<Navigate to="User" />} /> */}
        <Route path="/Employer" element={<EmployerPage />} />
        <Route path="/Profile" element={<ProfilePage />} />
        <Route path="/add/admin" element={<Addadmin />} />
        <Route path="/add/user" element={<Adduser />} />
        <Route path="/admin/:id" element={<Adminpage />} />
        <Route path="/login" element={<LoginPage />} />
      </Routes>
    </div>
    //   <FooterPage />
    // </div>
    //   </div>
    // </div>
  );
}

export default App;
