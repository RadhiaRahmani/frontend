import React from "react";
import img1 from "../assets/images/faces/face1.jpg";
import { Link } from "react-router-dom";
import { useEffect } from "react";
import { useState } from "react";
import { useParams } from "react-router-dom";
export default function SidebarPage() {
  const { iduser } = useParams();

  const [user, setuser] = useState([]);
  return (
    <nav class="sidebar sidebar-offcanvas" id="sidebar">
      <ul class="nav">
        <li class="nav-item nav-profile">
          <a href="#" class="nav-link">
            <div class="nav-profile-image">
              <img src={img1} alt="profile" />
              <span class="login-status online"></span>
            </div>
            <div class="nav-profile-text d-flex flex-column">
              <span class="font-weight-bold mb-2">إسم صاحب الحساب هنا</span>
              <span class="text-secondary text-small">يمكنك إضافة وظيفته</span>
            </div>
          </a>
        </li>
        <li class="nav-item">
          <Link to="/User" class="nav-link">
            <span class="menu-title">الحرفاء</span>
            <i class="mdi mdi-home menu-icon"></i>
          </Link>
        </li>
        <li class="nav-item">
          <Link to="/Employer" class="nav-link">
            <span class="menu-title">العملاء</span>
            <i class="mdi mdi-home menu-icon"></i>
          </Link>
        </li>
        <li class="nav-item">
          <Link to="/Profile" class="nav-link">
            <span class="menu-title">الحساب</span>
            <i class="mdi mdi-contacts menu-icon"></i>
          </Link>
        </li>
        <li class="nav-item">
          <Link to="/SignOut" class="nav-link">
            <span class="menu-title">تسجيل الخروج</span>
            <i class="mdi mdi-contacts menu-icon"></i>
          </Link>
        </li>
      </ul>
    </nav>
  );
}
