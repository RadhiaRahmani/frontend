import React from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import img1 from '../../assets/images/faces/face1.jpg';
import img2 from '../../assets/images/faces/face2.jpg';
import img3 from '../../assets/images/faces/face3.jpg';
import img4 from '../../assets/images/faces/face4.jpg';
import { BsXOctagonFill } from "react-icons/bs";
import { BiCheck } from "react-icons/bi";
import { AiFillDelete } from "react-icons/ai";
import { AiTwotoneEdit } from "react-icons/ai";
import { Modal, Button } from "react-bootstrap";
import AddUser from '../User/Adduser';
export default function  EmployerPage() {
    const [data, setdata] = useState([]);
      const [show, setShow] = useState(false);
      const handleClose = () => setShow(false);
      const handleShow = () => setShow(true);
    // get users function
    const getdata = async () => {
      await fetch("https://backend-dashboard-hxdw.vercel.app/employes", {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      })
        .then((response) => response.json())
        .then((response) => setdata(response))
        .then((error) => console.log(error));
    };
    // delete user function 
    const deleteuser = async (e, id) => {
      e.preventDefault();
      await fetch(
        `https://backend-dashboard-hxdw.vercel.app/delete/user/${id}`,
        {
          method: "DELETE",
          headers: {
            "Content-Type": "application/json",
          },
        }
      ).then((response) => {
        getdata();
        document.getElementById("successbox").style.visibility = "visible";
      });
    };
    // useeffcet
    useEffect(() => {
      getdata();
    }, []);
    return (
      <div class="row">
        <div class="col-12 grid-margin">
          <div class="card">
            <div class="card-body">
              <Button variant="primary" onClick={handleShow}>
                Add new employe
              </Button>

              <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                  <Modal.Title>Add new User</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  <AddUser />
                </Modal.Body>
                <Modal.Footer></Modal.Footer>
              </Modal>
              <h4 class="card-title text-center">Employes List</h4>
              <div class="table-responsive" style={{ marginTop: "50px" }}>
                <table class="table">
                  <thead>
                    <tr>
                      <th> User </th>
                      <th> Email </th>
                      <th> Phone Number </th>
                      <th> Creation Date </th>
                      <th> Verified </th>
                      <th> </th>
                      <th> </th>
                    </tr>
                  </thead>
                  {data.map((element) => {
                    return (
                      <>
                        <tbody>
                          <tr>
                            <td>
                              <img
                                src={element.image_profile}
                                class="me-2"
                                alt="image"
                              />{" "}
                              {element.user_firstname}
                              {element.user_lastname}
                            </td>
                            <td>{element.user_email}</td>
                            <td>{element.user_phone}</td>
                            <td>{element.creation_date}</td>
                            {element.verified === true ? (
                              <BiCheck style={{ fontSize: "40px" }} />
                            ) : (
                              <BsXOctagonFill style={{ fontSize: "40px" }} />
                            )}
                            <td>
                              <AiFillDelete
                                onClick={(e) => deleteuser(e, element.user_id)}
                                style={{ fontSize: "20px" }}
                              />
                            </td>
                            <td>
                              <AiTwotoneEdit style={{ fontSize: "20px" }} />
                            </td>
                          </tr>
                        </tbody>
                      </>
                    );
                  })}
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
}