import logo from "../../assets/images/logo.svg";
import { useNavigate } from "react-router-dom";
import { useState } from "react";
import { useEffect } from "react";

export default function LoginPage() {
  const navigate = useNavigate();
  const [data, setdata] = useState([]);
  const [phone, setphone] = useState("");
  const [password, setpassword] = useState("");
  const [user, setuser] = useState([]);
  // login function
  const getdata = () => {
    fetch("https://backend-dashboard-hxdw.vercel.app/allusers", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((response) => setdata(response))
      .then((error) => console.log(error));
  };
  useEffect(() => {
    getdata();
  }, []);
  const login = (e) => {
    e.preventDefault();
    console.log(phone);
    console.log(password);
    const response = fetch("https://backend-dashboard-hxdw.vercel.app/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        phone: phone,
        password: password,
      }),
    })
      .then((response) => response.json())
      .then((response) => setdata(response));
    if (data[0].id_role === 1) {
      navigate(`/admin/${data[0].user_id}`);
    }
  };
  return (
    <div class="container-scroller rtl">
      <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth">
          <div class="row flex-grow">
            <div class="col-lg-4 mx-auto">
              <div
                class="auth-form-light text-left p-5"
                style={{ marginBottom: "350px" }}
              >
                <div class="brand-logo"></div>
                <h6 class="font-weight-light">Sign in to continue.</h6>
                <form class="pt-3">
                  <div class="form-group">
                    <input
                      type="text"
                      class="form-control form-control-lg"
                      id="exampleInputEmail1"
                      placeholder="Phone number"
                      onChange={(e) => setphone(e.target.value)}
                    />
                  </div>
                  <div class="form-group">
                    <input
                      type="password"
                      class="form-control form-control-lg"
                      id="exampleInputPassword1"
                      placeholder="Password"
                      onChange={(e) => setpassword(e.target.value)}
                    />
                  </div>
                  <div class="mt-3">
                    <button
                      class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn"
                      onClick={(e) => login(e)}
                    >
                      SIGN IN
                    </button>
                  </div>
                  <div class="my-2 d-flex justify-content-between align-items-center">
                    <a href="#" class="auth-link text-black">
                      Forgot password?
                    </a>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
