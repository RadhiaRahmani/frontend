import React from "react";
import { useNavigate } from "react-router-dom";
import { useState } from "react";
import { useEffect } from "react";
const Addadmin = () => {
  const navigate = useNavigate();
  const [firstname, setfirstname] = useState("");
  const [lastname, setlastname] = useState("");
  const [phone, setphone] = useState("");
  const [email, setemail] = useState("");
  const [password, setpassword] = useState("");
  // sign up admin
  const register = (e) => {
    console.log(firstname);
    console.log("pass" + password);
    console.log(lastname);
    console.log(phone);
    console.log(email);
    e.preventDefault();
    fetch("https://backend-dashboard-hxdw.vercel.app/add/admin", {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        firstname: firstname,
        lastname: lastname,
        email: email,
        phone: phone,
        password: password,
      }),
    }).then((response) => {
      navigate("/login");
    });
  };
  return (
    <div>
      <div class="container-scroller rtl" style={{ marginRight: "250px" }}>
        <div class="container-fluid page-body-wrapper full-page-wrapper">
          <div class="content-wrapper d-flex align-items-center auth">
            <div class="row flex-grow">
              <div class="col-lg-4 mx-auto">
                <div
                  class="auth-form-light text-left p-5"
                  style={{ marginBottom: "180px" }}
                >
                  <div class="brand-logo"></div>
                  <h6 class="font-weight-light text-center">
                    Add Admin or <a href="/login">Login</a>{" "}
                  </h6>
                  <form class="pt-3" onSubmit={(e) => register(e)}>
                    {/* // firstname */}
                    <div class="form-group">
                      <input
                        type="text"
                        class="form-control form-control-lg"
                        id="exampleInputEmail1"
                        placeholder="firstname"
                        onChange={(e) => setfirstname(e.target.value)}
                      />
                    </div>
                    {/* lastname */}
                    <div class="form-group">
                      <input
                        type="lastname"
                        class="form-control form-control-lg"
                        id="exampleInputPassword1"
                        placeholder="lastname"
                        onChange={(e) => setlastname(e.target.value)}
                      />
                    </div>
                    {/* email */}

                    <div class="form-group">
                      <input
                        type="email"
                        class="form-control form-control-lg"
                        id="exampleInputPassword1"
                        placeholder="email"
                        onChange={(e) => setemail(e.target.value)}
                      />
                    </div>
                    {/* phone */}
                    <div class="form-group">
                      <input
                        type="phone"
                        class="form-control form-control-lg"
                        id="exampleInputPassword1"
                        placeholder="phone"
                        onChange={(e) => setphone(e.target.value)}
                      />
                    </div>
                    {/* Password */}
                    <div class="form-group">
                      <input
                        type="password"
                        class="form-control form-control-lg"
                        id="exampleInputPassword1"
                        placeholder="password"
                        onChange={(e) => setpassword(e.target.value)}
                      />
                    </div>

                    <div class="mt-3">
                      <input type="submit" value="Add"></input>
                    </div>
                    <div class="my-2 d-flex justify-content-between align-items-center"></div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Addadmin;
