import React from "react";
import UserPage from "../User/UserPage";
import SidebarPage from "../../Components/SidebarPage";
import NavbarPage from "../../Components/NavbarPage";
import EmployerPage from "../Employer/EmployerPage";
import { useNavigate, useParams } from "react-router-dom";
import { Link } from "react-router-dom";
import { useState } from "react";
import { useEffect } from "react";
export default function Adminpage() {
  // useparams
  const { id } = useParams();

  // use navigate
  const navigate = useNavigate();
  const [user, setuser] = useState(false);
  const [employe, setemploye] = useState(false);
  const [data, setdata] = useState("");
  // functions
  const showusers = (e) => {
    e.preventDefault();
    setuser(true);
    setemploye(false);
  };
  const showemployes = (e) => {
    e.preventDefault();
    setuser(false);
    setemploye(true);
  };
  const signout = (e) => {
    e.preventDefault();
    navigate("/login");
  };
  // get user
  const getuser = () => {
    fetch(`https://backend-dashboard-hxdw.vercel.app/users/${id}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((response) => setdata(response[0]));
  };
  // useeffect
  useEffect(() => {
    getuser();
    console.log(data);
  }, []);

  return (
    <div class="container-scroller rtl">
      <NavbarPage id={id} />
      <div class="container-fluid page-body-wrapper">
        <nav class="sidebar sidebar-offcanvas" id="sidebar">
          <ul class="nav">
            <li class="nav-item nav-profile">
              <a href="#" class="nav-link">
                <div class="nav-profile-image">
                  <img src={data.image_profile} alt="profile" />
                  <span class="login-status online"></span>
                </div>
                <div class="nav-profile-text d-flex flex-column">
                  <span class="font-weight-bold mb-2">إسم صاحب الحساب هنا</span>
                  <span class="text-secondary text-small">
                    يمكنك إضافة وظيفته
                  </span>
                </div>
              </a>
            </li>
            <li class="nav-item">
              <button
                className="btn btn-dark mt-3"
                onClick={(e) => showusers(e)}
              >
                <span class="menu-title">الحرفاء</span>
                <i class="mdi mdi-home menu-icon"></i>
              </button>
            </li>
            <li class="nav-item">
              <button
                className="btn btn-dark mt-3"
                onClick={(e) => showemployes(e)}
              >
                <span class="menu-title">العملاء</span>
                <i class="mdi mdi-home menu-icon"></i>
              </button>
            </li>
            {/* <li class="nav-item">
              <Link to="/Profile" class="nav-link">
                <span class="menu-title">الحساب</span>
                <i class="mdi mdi-contacts menu-icon"></i>
              </Link>
            </li> */}
            <li class="nav-item">
              <button className="btn btn-dark mt-3" onClick={(e) => signout(e)}>
                <span class="menu-title">تسجيل الخروج</span>
                <i class="mdi mdi-contacts menu-icon"></i>
              </button>
            </li>
          </ul>
        </nav>
        <div class="main-panel">
          <div class="content-wrapper">
            {user && <UserPage />}
            {employe && <EmployerPage />}
          </div>
        </div>
      </div>
    </div>
  );
}
