import React from "react";
import { useState } from "react";
import { useEffect } from "react";
import img1 from "../../assets/images/faces/face1.jpg";
import img2 from "../../assets/images/faces/face2.jpg";
import img3 from "../../assets/images/faces/face3.jpg";
import img4 from "../../assets/images/faces/face4.jpg";
import { BsXOctagonFill } from "react-icons/bs";
import { BiCheck } from "react-icons/bi";
import { AiFillDelete } from "react-icons/ai";
import { AiTwotoneEdit } from "react-icons/ai";
import { HiUserAdd } from "react-icons/hi";
import { useParams } from "react-router-dom";
import SidebarPage from "../../Components/SidebarPage";
import { Modal, Button } from "react-bootstrap";
import AddUser from "./Adduser";
import NavbarPage from "../../Components/NavbarPage";
export default function HomePage() {
  const { iduser } = useParams();
  // states
  const [data, setdata] = useState([]);
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  // get users function
  const getdata = async () => {
    await fetch("https://backend-dashboard-hxdw.vercel.app/users", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((response) => setdata(response))
      .then((error) => console.log(error));
  };
  // delete user  function
  const deleteuser = async (e, id) => {
    e.preventDefault();
    await fetch(`https://backend-dashboard-hxdw.vercel.app/delete/user/${id}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    }).then((response) => {
      getdata();
      document.getElementById("successbox").style.visibility = "visible";
    });
  };
  // edit user

  // useEffect
  useEffect(() => {
    getdata();
  }, []);
  return (
    
    <div class="row">
      <div class="col-12 grid-margin" style={{ position: "relative" }}>
        <div class="card">
          <div class="card-body">
            {/* add user */}
            <Button variant="primary" onClick={handleShow}>
              Add new user
            </Button>

            <Modal show={show} onHide={handleClose}>
              <Modal.Header closeButton>
                <Modal.Title>Add new User</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <AddUser />
              </Modal.Body>
              <Modal.Footer></Modal.Footer>
            </Modal>
            <h4 class="card-title text-center">Users List</h4>

            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th> User </th>
                    <th> Email </th>
                    <th> Phone Number </th>
                    <th> Creation Date </th>
                    <th> Verified </th>
                    <th> </th>
                    <th> </th>
                  </tr>
                </thead>
                {data.map((element) => {
                  return (
                    <>
                      <tbody>
                        <tr>
                          <td>
                            <img
                              src={element.image_profile}
                              class="me-2"
                              alt="image"
                            />{" "}
                            {element.user_firstname}
                            {element.user_lastname}
                          </td>
                          <td>{element.user_email}</td>
                          <td>{element.user_phone}</td>
                          <td>{element.creation_date}</td>
                          {element.verified === true ? (
                            <BiCheck style={{ fontSize: "40px" }} />
                          ) : (
                            <BsXOctagonFill style={{ fontSize: "40px" }} />
                          )}
                          <td>
                            <AiFillDelete
                              onClick={(e) => deleteuser(e, element.user_id)}
                              style={{ fontSize: "20px" }}
                            />
                          </td>
                          <td>
                            <AiTwotoneEdit style={{ fontSize: "20px" }} />
                          </td>
                        </tr>
                      </tbody>
                    </>
                  );
                })}
              </table>
            </div>
          </div>
        </div>
      </div>

      {/* <div class="col-lg-6 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title text-center">Add User</h4>

            <div class="modal" tabindex="-1" role="dialog">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title">Modal title</h5>
                    <button
                      type="button"
                      class="close"
                      data-dismiss="modal"
                      aria-label="Close"
                    >
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <p>Modal body text goes here.</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-primary">
                      Save changes
                    </button>
                    <button
                      type="button"
                      class="btn btn-secondary"
                      data-dismiss="modal"
                    >
                      Close
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Striped Table</h4>
            <p class="card-description">
              {" "}
              Add class <code>.table-striped</code>
            </p>
            <table class="table table-striped">
              <thead>
                <tr>
                  <th> User </th>
                  <th> First name </th>
                  <th> Progress </th>
                  <th> Amount </th>
                  <th> Deadline </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="py-1">
                    <img src={img1} alt="image" />
                  </td>
                  <td> Herman Beck </td>
                  <td>
                    <div class="progress">
                      <div
                        class="progress-bar bg-success"
                        role="progressbar"
                        style={{ width: "25%" }}
                        aria-valuenow="25"
                        aria-valuemin="0"
                        aria-valuemax="100"
                      ></div>
                    </div>
                  </td>
                  <td> $ 77.99 </td>
                  <td> May 15, 2015 </td>
                </tr>
                <tr>
                  <td class="py-1">
                    <img src={img2} alt="image" />
                  </td>
                  <td> Messsy Adam </td>
                  <td>
                    <div class="progress">
                      <div
                        class="progress-bar bg-danger"
                        role="progressbar"
                        style={{ width: "75%" }}
                        aria-valuenow="75"
                        aria-valuemin="0"
                        aria-valuemax="100"
                      ></div>
                    </div>
                  </td>
                  <td> $245.30 </td>
                  <td> July 1, 2015 </td>
                </tr>
                <tr>
                  <td class="py-1">
                    <img src={img3} alt="image" />
                  </td>
                  <td> John Richards </td>
                  <td>
                    <div class="progress">
                      <div
                        class="progress-bar bg-warning"
                        role="progressbar"
                        style={{ width: "90%" }}
                        aria-valuenow="90"
                        aria-valuemin="0"
                        aria-valuemax="100"
                      ></div>
                    </div>
                  </td>
                  <td> $138.00 </td>
                  <td> Apr 12, 2015 </td>
                </tr>
                <tr>
                  <td class="py-1">
                    <img src={img4} alt="image" />
                  </td>
                  <td> Peter Meggik </td>
                  <td>
                    <div class="progress">
                      <div
                        class="progress-bar bg-primary"
                        role="progressbar"
                        style={{ width: "50%" }}
                        aria-valuenow="50"
                        aria-valuemin="0"
                        aria-valuemax="100"
                      ></div>
                    </div>
                  </td>
                  <td> $ 77.99 </td>
                  <td> May 15, 2015 </td>
                </tr>
                <tr>
                  <td class="py-1">
                    <img src={img1} alt="image" />
                  </td>
                  <td> Edward </td>
                  <td>
                    <div class="progress">
                      <div
                        class="progress-bar bg-danger"
                        role="progressbar"
                        style={{ width: "35%" }}
                        aria-valuenow="35"
                        aria-valuemin="0"
                        aria-valuemax="100"
                      ></div>
                    </div>
                  </td>
                  <td> $ 160.25 </td>
                  <td> May 03, 2015 </td>
                </tr>
                <tr>
                  <td class="py-1">
                    <img src={img2} alt="image" />
                  </td>
                  <td> John Doe </td>
                  <td>
                    <div class="progress">
                      <div
                        class="progress-bar bg-info"
                        role="progressbar"
                        style={{ width: "65%" }}
                        aria-valuenow="65"
                        aria-valuemin="0"
                        aria-valuemax="100"
                      ></div>
                    </div>
                  </td>
                  <td> $ 123.21 </td>
                  <td> April 05, 2015 </td>
                </tr>
                <tr>
                  <td class="py-1">
                    <img src={img3} alt="image" />
                  </td>
                  <td> Henry Tom </td>
                  <td>
                    <div class="progress">
                      <div
                        class="progress-bar bg-warning"
                        role="progressbar"
                        style={{ width: "20%" }}
                        aria-valuenow="20"
                        aria-valuemin="0"
                        aria-valuemax="100"
                      ></div>
                    </div>
                  </td>
                  <td> $ 150.00 </td>
                  <td> June 16, 2015 </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Bordered table</h4>
            <p class="card-description">
              {" "}
              Add class <code>.table-bordered</code>
            </p>
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th> # </th>
                  <th> First name </th>
                  <th> Progress </th>
                  <th> Amount </th>
                  <th> Deadline </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td> 1 </td>
                  <td> Herman Beck </td>
                  <td>
                    <div class="progress">
                      <div
                        class="progress-bar bg-success"
                        role="progressbar"
                        style={{ width: "25%" }}
                        aria-valuenow="25"
                        aria-valuemin="0"
                        aria-valuemax="100"
                      ></div>
                    </div>
                  </td>
                  <td> $ 77.99 </td>
                  <td> May 15, 2015 </td>
                </tr>
                <tr>
                  <td> 2 </td>
                  <td> Messsy Adam </td>
                  <td>
                    <div class="progress">
                      <div
                        class="progress-bar bg-danger"
                        role="progressbar"
                        style={{ width: "75%" }}
                        aria-valuenow="75"
                        aria-valuemin="0"
                        aria-valuemax="100"
                      ></div>
                    </div>
                  </td>
                  <td> $245.30 </td>
                  <td> July 1, 2015 </td>
                </tr>
                <tr>
                  <td> 3 </td>
                  <td> John Richards </td>
                  <td>
                    <div class="progress">
                      <div
                        class="progress-bar bg-warning"
                        role="progressbar"
                        style={{ width: "90%" }}
                        aria-valuenow="90"
                        aria-valuemin="0"
                        aria-valuemax="100"
                      ></div>
                    </div>
                  </td>
                  <td> $138.00 </td>
                  <td> Apr 12, 2015 </td>
                </tr>
                <tr>
                  <td> 4 </td>
                  <td> Peter Meggik </td>
                  <td>
                    <div class="progress">
                      <div
                        class="progress-bar bg-primary"
                        role="progressbar"
                        style={{ width: "50%" }}
                        aria-valuenow="50"
                        aria-valuemin="0"
                        aria-valuemax="100"
                      ></div>
                    </div>
                  </td>
                  <td> $ 77.99 </td>
                  <td> May 15, 2015 </td>
                </tr>
                <tr>
                  <td> 5 </td>
                  <td> Edward </td>
                  <td>
                    <div class="progress">
                      <div
                        class="progress-bar bg-danger"
                        role="progressbar"
                        style={{ width: "35%" }}
                        aria-valuenow="35"
                        aria-valuemin="0"
                        aria-valuemax="100"
                      ></div>
                    </div>
                  </td>
                  <td> $ 160.25 </td>
                  <td> May 03, 2015 </td>
                </tr>
                <tr>
                  <td> 6 </td>
                  <td> John Doe </td>
                  <td>
                    <div class="progress">
                      <div
                        class="progress-bar bg-info"
                        role="progressbar"
                        style={{ width: "65%" }}
                        aria-valuenow="65"
                        aria-valuemin="0"
                        aria-valuemax="100"
                      ></div>
                    </div>
                  </td>
                  <td> $ 123.21 </td>
                  <td> April 05, 2015 </td>
                </tr>
                <tr>
                  <td> 7 </td>
                  <td> Henry Tom </td>
                  <td>
                    <div class="progress">
                      <div
                        class="progress-bar bg-warning"
                        role="progressbar"
                        style={{ width: "20%" }}
                        aria-valuenow="20"
                        aria-valuemin="0"
                        aria-valuemax="100"
                      ></div>
                    </div>
                  </td>
                  <td> $ 150.00 </td>
                  <td> June 16, 2015 </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Inverse table</h4>
            <p class="card-description">
              {" "}
              Add class <code>.table-dark</code>
            </p>
            <table class="table table-dark">
              <thead>
                <tr>
                  <th> # </th>
                  <th> First name </th>
                  <th> Amount </th>
                  <th> Deadline </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td> 1 </td>
                  <td> Herman Beck </td>
                  <td> $ 77.99 </td>
                  <td> May 15, 2015 </td>
                </tr>
                <tr>
                  <td> 2 </td>
                  <td> Messsy Adam </td>
                  <td> $245.30 </td>
                  <td> July 1, 2015 </td>
                </tr>
                <tr>
                  <td> 3 </td>
                  <td> John Richards </td>
                  <td> $138.00 </td>
                  <td> Apr 12, 2015 </td>
                </tr>
                <tr>
                  <td> 4 </td>
                  <td> Peter Meggik </td>
                  <td> $ 77.99 </td>
                  <td> May 15, 2015 </td>
                </tr>
                <tr>
                  <td> 5 </td>
                  <td> Edward </td>
                  <td> $ 160.25 </td>
                  <td> May 03, 2015 </td>
                </tr>
                <tr>
                  <td> 6 </td>
                  <td> John Doe </td>
                  <td> $ 123.21 </td>
                  <td> April 05, 2015 </td>
                </tr>
                <tr>
                  <td> 7 </td>
                  <td> Henry Tom </td>
                  <td> $ 150.00 </td>
                  <td> June 16, 2015 </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div> */}
    </div>
    
  );
}
