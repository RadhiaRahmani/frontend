import React, { useState } from "react";
import DatePicker from "react-datepicker";
import { compareAsc, format } from "date-fns";
import { useEffect } from "react";
import { Country, State, City } from "country-state-city";
import { MDBSwitch } from "mdb-react-ui-kit";
import "react-datepicker/dist/react-datepicker.css";
export default function AddUser() {
  // states
  const [countries, setcountries] = useState([]);
  const [email, setemail] = useState(false);
  const [whatsapp, setwhatsapp] = useState(false);
  const [country, setcountry] = useState("");
  const [statename, setstatename] = useState("");
  const [cities, setcities] = useState([]);
  const [states, setstates] = useState([]);
  const [languagecontent, setlanguagecontent] = useState([]);
  const [today, settoday] = useState(new Date());
  const [listusers, setlistusers] = useState("");
  const [listemployes, setlistemployes] = useState("");
  const [addemploye, setaddemploye] = useState("");
  const [useradd, setuseradd] = useState("");
  const [deleteuser, setdeleteuser] = useState();
  const [deleteemploye, setdeleteemploye] = useState("");
  const [data, setdata] = useState([]);

  // to day date
  const today_date = format(today, "yyyy-MM-dd");
  const fulladress = country + "" + statename;

  // generate code
  const min = Math.ceil(1000);
  const max = Math.floor(2000);
  const code = Math.floor(Math.random() * (max - min) + min);
  // phone
  const [phone, setphone] = useState("");

  // localstorage
  const id_language = localStorage.getItem("language");
  // // get language content
  // const getlanguage = async () => {
  //   const response = await fetch(
  //     `http://localhost:3001/language/${id_language}`,
  //     {
  //       method: "GET",
  //       headers: {
  //         "Content-Type": "application/json",
  //       },
  //     }
  //   );
  //   const result = await response.json();
  //   setlanguagecontent(result[0]);
  // };
  // handle checkbox
  const HandleEmail = (event) => {
    if (event.target.checked) {
      setemail(true);
    }
  };
  const Handlewhatsapp = (event) => {
    if (event.target.checked) {
      setwhatsapp(true);
    }
  };
  const HandleListusers = (event) => {
    if (event.target.checked) {
      setlistusers(true);
    }
  };
  const HandleListEmployes = (event) => {
    if (event.target.checked) {
      setlistemployes(true);
    }
  };
  const HandleAddUser = (event) => {
    if (event.target.checked) {
      setuseradd(true);
    }
  };
  const HandleAddEmploye = (event) => {
    if (event.target.checked) {
      setaddemploye(true);
    }
  };
  const HandleDeleteEmploye = (event) => {
    if (event.target.checked) {
      setdeleteemploye(true);
    }
  };
  const HandleDeleteUsre = (event) => {
    if (event.target.checked) {
      setdeleteuser(true);
    }
  };

  const getcountry = () => {
    const ct = Country.getAllCountries();
    setcountries(ct);
  };
  const getstates = () => {
    const st = State.getAllStates();
    setstates(st);
  };
  const getcities = () => {
    const ct = City.getAllCities();
    setcities(ct);
  };
  useEffect(() => {
    getcountry();
    getstates();
    getcities();
    getdata();

    console.log(data);
    // getlanguage();
  }, []);
  // fetch states
  const fetchcountrie = (countrie) => {
    // const state = states.filter((element) => element.countryCode === "ET");
    // setstates(state);
    const ph = countries.find((element) => element.name === countrie);
    setphone(ph.phonecode);
    setcountry(ph.name);
    const st = states.filter((element) => element.countryCode === ph.isoCode);
    setstates(st);
  };

  const [startDate, setStartDate] = useState(new Date());
  const newdate = format(startDate, "yyyy-MM-dd");
  console.log("date" + newdate);
  const [user, setuser] = useState({
    firstname: "",
    lastname: "",
    email: "",
    civility: "",
    poste: "",
    birthday: "",
    phone: "",
    address: "",
    seniority: "",
    experience: "",
    comment: "",
    cin: "",
    role: 3,
  });
  // handle form changes
  const handlechange = (e) => {
    const { name, value } = e.target;
    setuser((element) => ({
      ...element,
      [name]: value,
    }));
  };
  // get all users
  const getdata = async () => {
    await fetch("https://backend-dashboard-hxdw.vercel.app/allusers", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((response) => setdata(response))
      .then((error) => console.log(error));
  };

  // get max id
  const max_id = Math.max(...data.map((o) => o.user_id)) + 1;
  console.log(max_id);
  // add a new user
  const adduser = async (e) => {
    getcountry();
    getstates();
    getcities();
    e.preventDefault();
    await fetch("https://backend-dashboard-hxdw.vercel.app/add/user/", {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        firstname: user.firstname,
        lastname: user.lastname,
        phone: phone,
        email: user.email,
        civility: user.civility,
        speciality: user.speciality,
        adress: fulladress,
        birthday: newdate,
        seniority: user.seniority,
        experience: user.experience,
        comment: user.comment,
        role: user.role,
        cin: user.cin,
        poste: user.poste,
        datecreation: today_date,
        id: max_id,
      }),
    }).then((response) => {
      // document.getElementById("alertmessage").style.visibility = "visible";
      e.target.reset();
    });

    if (email === true) {
      await fetch("https://send-email-dsas.vercel.app/send/email/", {
        method: "POST",
        headers: {
          "Content-type": "application/json",
        },
        body: JSON.stringify({
          email: user.email,
          code: code,
          url: `https://backend-dashboard-hxdw.vercel.app/verify/code/${max_id}`,
          datesend: newdate,
        }),
      }).then((response) => {});
    }
    if (whatsapp === true) {
      await fetch("https://send-email-dsas.vercel.app/send/whatssapp/", {
        method: "POST",
        headers: {
          "Content-type": "application/json",
        },
        body: JSON.stringify({
          email: user.email,
          code: code,
          url: `https://backend-dashboard-hxdw.vercel.app/verify/code/${max_id}`,
          datesend: newdate,
        }),
      }).then((response) => {});
    }
    if(listemployes===true){
      const id_permission=2;
      await fetch("https://backend-dashboard-hxdw.vercel.app/add/permissions", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          id_permission: id_permission,
          id: max_id,
        }),
      });
  }
  if (listusers === true) {
    const id_permission = 1;
    await fetch("https://backend-dashboard-hxdw.vercel.app/add/permissions", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
         id_permission:id_permission,
        id:max_id,
      }),
    });
  }
  if (useradd === true) {
    const id_permission = 7;
    await fetch("https://backend-dashboard-hxdw.vercel.app/add/permissions", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id_permission: id_permission,
        id: max_id,
      }),
    });
  }
  if (addemploye === true) {
    const id_permission = 8;
    await fetch("https://backend-dashboard-hxdw.vercel.app/add/permissions", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id_permission: id_permission,
        id: max_id,
      }),
    });
  }
  if (deleteemploye === true) {
    const id_permission = 4;
    await fetch("https://backend-dashboard-hxdw.vercel.app/add/permissions", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
         id_permission:id_permission,
        id:max_id,
      }),
    });
  }
  if (deleteuser === true) {
    const id_permission = 3;
    await fetch("https://backend-dashboard-hxdw.vercel.app/add/permissions", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id_permission:id_permission,
        id:max_id,
      }),
    });
  }
    
  };
  return (
    <div>
      <div className="">
        <h4>{languagecontent.text4}</h4>
        <form onSubmit={(e) => adduser(e)}>
          {/* nom */}
          <div class="form-group">
            <input
              type="text"
              class="form-control mt-3"
              placeholder="enter your firstname"
              name="firstname"
              onChange={handlechange}
              required="required"
            />
          </div>
          {/* prenom */}
          <div class="form-group mt-3">
            <input
              type="text"
              class="form-control"
              placeholder="enter your lastname"
              name="lastname"
              onChange={handlechange}
              required="required"
            />
            {/* civilite */}
            <div class="form-group mt-3">
              <input
                type="text"
                class="form-control"
                placeholder="enter your civility"
                name="civility"
                onChange={handlechange}
              />
            </div>
          </div>
          {/* adresee */}
          {/* email */}
          <div class="form-group mt-3">
            <input
              type="email"
              class="form-control"
              placeholder="enter your email"
              name="email"
              onChange={handlechange}
            />
          </div>
          {/* date de naissance */}
          <div class="form-group mt-3">
            <label>{languagecontent.col11}</label>
            <DatePicker
              placeholderText="add a date "
              className="form-control"
              selected={startDate}
              onChange={(date) => setStartDate(date)}
            />
          </div>
          {/* adresse */}
          <div className="mt-4">
            {countries && (
              <select
                className="form-select"
                onChange={(e) => fetchcountrie(e.target.value)}
              >
                <option selected hidden disabled>
                  {languagecontent.text5}
                </option>
                {countries.map((country) => (
                  <option value={country.name}>{country.name}</option>
                ))}
              </select>
            )}
            {states && (
              <select
                className="form-select mt-3"
                onChange={(e) => setstatename(e.target.value)}
              >
                <option selected hidden disabled>
                  {languagecontent.text6}
                </option>
                {states.map((state) => (
                  <option value={state.name}>{state.name}</option>
                ))}
              </select>
            )}
            <div></div>
            {/* phone */}
          </div>
          <div class="form-group mt-3">
            <input
              type="text"
              class="form-control"
              name="phone"
              onChange={(e) => setphone(e.target.value)}
              defaultValue={phone}
              placeholder="enter your phone"
              required
            />
          </div>
          <div class="form-group mt-3">
            <input
              type="text"
              class="form-control"
              name=" entrer votre address"
              onChange={handlechange}
              defaultValue={country + statename}
              placeholder="enter your adress"
            />
          </div>
          <div class="form-group mt-3">
            <input
              type="text"
              class="form-control"
              name="speciality"
              onChange={handlechange}
              placeholder="enter your speciality"
            />
          </div>
          <div class="form-group mt-3">
            <input
              type="text"
              class="form-control"
              name="seniority"
              onChange={handlechange}
              placeholder="enter your seniority"
            />
          </div>
          {/* send button */}
          <div className="d-flex">
            <div style={{ display: "flex" }}>
              {" "}
              <div class="form-check form-switch">
                <input
                  class="form-check-input"
                  type="checkbox"
                  role="switch"
                  id="whatsapp"
                  name="whatsapp"
                  checked
                  onChange={(e) => Handlewhatsapp(e)}
                />
                <label
                  class="form-check-label d-flex"
                  checked
                  for="flexSwitchCheckDefault"
                >
                  Whatsapp
                </label>
              </div>
              <div
                class="form-check form-switch d-flex"
                style={{ marginLeft: "20px" }}
              >
                <input
                  class="form-check-input"
                  type="checkbox"
                  role="switch"
                  id="email"
                  name="email"
                  onChange={(e) => HandleEmail(e)}
                />
                <label class="form-check-label" for="flexSwitchCheckDefault">
                  Email
                </label>
              </div>
            </div>
          </div>

          <br />
          <label>Permissions:</label>
          <ul className="list-group">
            <div
              class="form-check form-switch d-flex"
              style={{ marginLeft: "20px" }}
            >
              <input
                class="form-check-input"
                type="checkbox"
                role="switch"
                id="email"
                name="email"
                checked
                onChange={(e) => HandleListusers(e)}
              />
              <label class="form-check-label" for="flexSwitchCheckDefault">
                List Users
              </label>
            </div>
            <div
              class="form-check form-switch d-flex"
              style={{ marginLeft: "20px" }}
            >
              <input
                class="form-check-input"
                type="checkbox"
                role="switch"
                id="email"
                name="email"
                checked
                onChange={(e) => HandleListEmployes(e)}
              />
              <label class="form-check-label" for="flexSwitchCheckDefault">
                List Employes
              </label>
            </div>
            <div
              class="form-check form-switch d-flex"
              style={{ marginLeft: "20px" }}
            >
              <input
                class="form-check-input"
                type="checkbox"
                role="switch"
                id="email"
                name="email"
                onChange={(e) => HandleAddUser(e)}
                checked
              />
              <label class="form-check-label" for="flexSwitchCheckDefault">
                Add User
              </label>
            </div>
            <div
              class="form-check form-switch d-flex"
              style={{ marginLeft: "20px" }}
            >
              <input
                class="form-check-input"
                type="checkbox"
                role="switch"
                id="email"
                name="email"
                onChange={(e) => HandleAddEmploye(e)}
                checked
              />
              <label class="form-check-label" for="flexSwitchCheckDefault">
                Add Employe
              </label>
            </div>
            <div
              class="form-check form-switch d-flex"
              style={{ marginLeft: "20px" }}
            >
              <input
                class="form-check-input"
                type="checkbox"
                role="switch"
                id="email"
                name="email"
                onChange={(e) => HandleDeleteEmploye(e)}
                checked
              />
              <label class="form-check-label" for="flexSwitchCheckDefault">
                Delete Employe
              </label>
            </div>
            <div
              class="form-check form-switch d-flex"
              style={{ marginLeft: "20px" }}
            >
              <input
                class="form-check-input"
                type="checkbox"
                role="switch"
                id="email"
                name="email"
                onChange={(e) => HandleDeleteUsre(e)}
                checked
              />
              <label class="form-check-label" for="flexSwitchCheckDefault">
                Delete user
              </label>
            </div>
          </ul>
          <div className="" style={{ marginLeft: "140px", marginTop: "20px" }}>
            <input type="submit" className="btn btn-primary" value="SEND" />
          </div>
        </form>
        {/* <div
          class="alert alert-secondary text-success  "
          id="alertmessage"
          style={{ visibility: "hidden" }}
          role="alert"
        >
          <i class="fa-solid fa-circle-check"></i>
        </div> */}
      </div>
    </div>
  );
}
